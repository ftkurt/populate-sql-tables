# Populate SQL Tables
Populate SQL tables with randomly created data

This lets you populate a table with randomly generated data with different types.

Here is a [demo](https://dl.dropboxusercontent.com/u/87240333/WEB/libs/randomwords/test.html).

# Usage

After you start page, you type table title, number of tuples. And then add attributes by typing attribute name choosing attribute type from menu and adding more new attributes by clicking "Add New Attribute" button.
